#!/bin/sh

# compile java files to class files
javac -classpath src -d build src/projekt/board/*.java
javac -classpath src -d build src/projekt/ui/*.java
javac -classpath src -d build -cp build src/projekt/MainApplication.java

# compress class files to jar
ulimit -v unlimited
jar cvfe dest-client/ija2015-client.jar projekt.MainApplication -C build/ .

# run program
java -jar dest-client/ija2015-client.jar