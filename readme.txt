IJA: Projekt Labyrinth
skupina: grp249
členové: Zdeněk Studený [xstude21], Martin Nosek - Nepodílel se na realizaci. 

Cílem bylo vytvořit aplikaci, simulující deskovou hru Labyrinth.
Zvolil jsem variantu A, tedy lokální hru pro více hráčů s možností undo. 
Projekt je objektove orientovany a při jeho tvorbe jsem vycházel z již vytvořených částí, které jsme měli dříve za domácí úkol. Byl to dobrý základ a velice si cením struktury tohoto předmětu. Dále se implementovala třída Player pro hráče, Game pro hru a jeji průběh a grafické rozhraní.
Bohužel jsem byl na celý projekt sám a nestihl jsem implementovat ukládání hry a UNDO, vše ostatní funguje.