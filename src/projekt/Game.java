package projekt;


import java.util.ArrayList;
import java.util.List;
import projekt.board.MazeBoard;
import projekt.board.MazeField;
import projekt.gui.InGameUI;
import projekt.player.Player;
import projekt.treasure.CardPack;
import projekt.treasure.Treasure;

/**
* Trida predstavuje hru Labyrinthu.
* Vyuziva temer vsechny ostatni tridy a sjednocuje je do hry Labyrintu.
* Nepredpoklada se pouzivani teto tridy pro vice her zaroven, informace jsou tak
* ulozeny v statickych promennych.
* Obsahuje objekty, ktere jsou potreba take u stolni hry, jako hraci plochu,
* hrace, balicek karet.
* V actPlayer je uschovano cislo(podle poradi) aktualniho hrace. 
* @author xstude21
*/
public class Game{
    public static MazeBoard board = null;
    public static List<Player> player = new ArrayList<>();
    public static CardPack pack;
    private static int actPlayer;
    private static MazeField shifted = null;
    private static MazeField blocked = null;
    public static boolean playerShiffted = false;
    
    /**
     * Nastaveni Game do pocatecni pozice, novy stul a hraci na mistech.
     * Vola se inicializace stolu, vytvori se set pokladu a hraci se
     * inicializuji a je jim prirazena pocatecni pozice.
     * Pote je hra zapocata a zacina ji prvni hrac.
     * @param boardSize - velikost hraci plochy.
     * @param treasures - pocet pokladu.
     * @param players - pocat hracu.
     */
    public static void newGame(int boardSize, int treasures, int players){
        board = MazeBoard.createMazeBoard(boardSize, treasures);
        Treasure.createSet(treasures);
        board.newGame();
        
        pack = new CardPack(treasures, treasures);
        pack.shuffle();
        
        for(int i = 0; i < players; ++i){
            switch(i){
                case 0:
                    player.add(new Player(board.get(1,1)));
                    break;
                case 1:
                    player.add(new Player(board.get(1, board.getSize())));
                    break;
                case 2:
                    player.add(new Player(board.get(board.getSize(), board.getSize())));
                    break;
                case 3:
                    player.add(new Player(board.get(board.getSize(), 1)));
                    break;
            }
        }
        actPlayer = 0;
        shifted = null;
        blocked = null;
        playerTurn();
        
        InGameUI.setBoard(board);
        
    }
    
    /**
     * Vrací zablokovanou kartu.
     * @return Blokovaná karta
     */
    public static MazeField getBlocked(){
       return Game.blocked;
    }
    
    /**
     * Tah hrace.
     * Jsou aktualizovany promenne, kam muze hrac vkladat kamen a kam se muze
     * pohnout. 
     */
    public static void playerTurn(){
        blocked = blockShift(shifted);
        shifted = null;
        playerShiffted = false;
        board.showRoutes(getActPlayer().getLocationRow()-1, getActPlayer().getLocationCol()-1);
        if(getActPlayer().getActTreasure() == null)
            getActPlayer().getCard(pack.popCard());
        
    }
    
    /**
     * Konec tahu hrace a zavolani dalsiho hrace.
     * Funkce je volana az pote, co se hrac uz pohnul. Je tak nejprve
     * zkontrlovane, jestli neziskal poklad, pote se jeste kontroluje, zda
     * nevyhral. Nakonec je prepnuto na dalsiho hrace a vola se playerTurn().
     */
    public static void playerNext(){
        getActPlayer().checkTreasure();
        if(getActPlayer().won()){
            InGameUI.playerWon(getActPlayer());
        }
        if(getActPlayer().getActTreasure() == null)
            getActPlayer().getCard(pack.popCard());
        actPlayer =  (actPlayer + 1) % getNumberOfPlayers();
        System.out.println("ActPlayer "+ actPlayer);
        playerTurn();
    }
    
    public static void playerPrev(MazeField blocked){
        actPlayer = (actPlayer - 1 + getNumberOfPlayers())  % getNumberOfPlayers();
        Game.blocked = blocked;
    }
    
    /**
     * Ziska hrace podle jeho poradi ve hre.
     * @param position - poradi hrace.
     * @return hrac
     */
    public static Player getPlayer(int position){
        if(position < player.size() && position >= 0)
            return player.get(position);
        return null;
    }
  
    /**
     * Vraci pocet hracu.
     * @return pocet hracu.
     */
    public static int getNumberOfPlayers(){
        return player.size();
    }
    
    /**
     * Vraci aktualniho hrace
     * @return aktualni hrac.
     */
    public static Player getActPlayer(){
        return getPlayer( actPlayer );
    }
    
    /**
     * Ulozi si misto, kam byl kamen vlozen.
     * @param field vlozene misto.
     */
    public static void setShifted(MazeField field){
        shifted = field;
        playerShiffted = true;
    }
    
    /**
     * Vynuluje ulozeni mista kamene.
     */
    public static void unsetShifted(){
        shifted = null;
        playerShiffted = false;
    }
    /**
     * Hrac muze vkladat, pokud tim nebude reverzovat predesly tah.
     * @param field lozene misto.
     * @return Jestli se na dane pole da vkladat.
     */
    public static boolean canShift(MazeField field){
        return (shifted == null && field != blocked);
    }
    
    /**
     * Vraci pole, kam je treba vlozit, aby byl proveden reverzni tah.
     * @param field - Reverzni pole.
     * @return 
     */
    public static MazeField blockShift(MazeField field){
        if ( field != null){
            if ( field.col() % 2 == 0 && field.row() == 1)
                return board.get(board.getSize(), field.col());
            if ( field.col() % 2 == 0 && field.row() == board.getSize())
                return board.get(1, field.col());
            if ( field.col() == 1 && field.row() % 2 == 0)
                return board.get(field.row(), board.getSize());
            if ( field.col() == board.getSize() && field.row() % 2 == 0)
                return board.get(field.row(), 1);           
                
        }
        return null;
    }
    
    /**
     * Ulozi hru - nestihnul jsem implementovat.
     */
    public static void saveGame(){
        
        /*try{
            // Serialize data object to a file
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("MyObject.ser"));
            out.writeObject( board);
            out.close();
            System.out.println("Pack serialized Savid board :"+ board.getFreeCard().getSymbol());

        } catch (IOException e) {
            System.err.println("Serialization failed "+ e);
        }
        
        
        
        try{
            FileInputStream door = new FileInputStream("MyObject.ser");
            ObjectInputStream reader = new ObjectInputStream(door);
            MazeBoard loadedBoard;
            loadedBoard = (MazeBoard) reader.readObject();
            board = loadedBoard;
            
            System.out.println("Loaded board :"+ loadedBoard.getFreeCard().getSymbol());
        }catch ( IOException e){
                e.printStackTrace();
        }
                */
    }
    
}
