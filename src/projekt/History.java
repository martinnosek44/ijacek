/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekt;

import java.util.ArrayList;
import java.util.List;
import projekt.board.MazeCard;
import projekt.board.MazeField;
import projekt.player.Player;

/**
 *
 * @author xstude21, xnosek10
 */
public class History {
    private static List<History> stack = new ArrayList<>();
    private static MazeField field = null;
    private static char symbol = ' ';
    private MazeField tmpField;
    private char tmpSymbol;
    private MazeField playerPrevPosition;
    private MazeField blocked;
    private boolean treasurePicked;
    
    
    private History(Player player, MazeField blocked, MazeField destination){
        this.tmpField = field;
        this.tmpSymbol = symbol;
        field = null;
        this.playerPrevPosition = player.getLocation();
        this.blocked = blocked;
        treasurePicked = destination.getCard().getTreasure() == player.getActTreasure();
    }
    
    public static void shift(MazeField field, char symbol){
        History.field = Game.blockShift(field);
        History.symbol = symbol;
    }
    
    public static void move(Player player, MazeField blocked, MazeField destination){
        stack.add(new History(player, blocked, destination));
    }
    
    public static void undo(){
        if(field != null){
            
            while(symbol != Game.board.getFreeCard().getSymbol()){
                Game.board.getFreeCard().turnRight();
            }
            Game.board.shift(field);
            Game.unsetShifted();
            field = null;
        }else if (!stack.isEmpty()){
            History record = stack.get(stack.size()-1);
            stack.remove(stack.size()-1);
            Game.playerPrev(record.blocked);
            
            Game.getActPlayer().setLocation(record.playerPrevPosition);
            field = record.tmpField;
            symbol = record.tmpSymbol;
            if(record.treasurePicked)
                Game.getActPlayer().dropTreasure();
            
            Game.setShifted(field);
        }
        
          
    }
    
}
