package projekt.board;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import projekt.Game;
import projekt.player.Player;
import projekt.treasure.Treasure;

/**
 * Trida popisuje hraci stul.
 * Ten je reprezentovan jako 2D pole MazeField, dale obsahuje take volnou hraci
 * kartu. 2D pole canGo slouzi pro zjisteni cest, kam se hrac muze vydat. Jakoby
 * kopiruje boardSpace, obsahuje vsak jen boolean hodnoty.
 * @author xstude21
 */

public class MazeBoard{
  
  private final MazeField[][] boardSpace;
  private int size;
  private int treasureNumber;
  private MazeCard freeCard;
  private boolean[][] canGo;
  
  private MazeBoard(int size){
      this.size = size;
      this.boardSpace = new MazeField[size][size];
      this.freeCard = null;
  }
  
  public static MazeBoard createMazeBoard(int n, int treasureNumber){
    MazeBoard board = new MazeBoard(n);
    board.size = n;
    board.treasureNumber = treasureNumber;
    board.canGo = new boolean[n][n];
    board.freeCard = null;
    
    for(int i = 1; i <= n; i++)
    {
      for(int j = 1; j <= n; j++)
      {
        board.boardSpace[i-1][j-1] = new MazeField(i,j);
        board.canGo[i-1][j-1] = false;
      }
    }
    return board;   
  }
  /**
   * Funkce vraci velikost boardu.
   * (Neni to celkovy pocet poli, ale delka sloupce/rady)
   * @return Velikost hry
   */
  public int getSize(){
      return this.size;
  }
  
  /**
   * Vytvori novou hru.
   * Vytvori a rozmisti hraci kameny po plose.
   * Kameny, ktere maji byt rozmisteny nahodne jsou nejprve vlozeny do pomocneho
   * pole bagOfCards, zamichany a nahodne pootoceny. Pote jsou rozdany na volne
   * mista. Nakonec jsou nahodne prideleny poklady jednotlivym hracim kamenum.
   */
  public void newGame(){
     int r;
     MazeCard card;
     MazeCard bagOfCards[];
     MazeCard shuffleCard;
     Random magic = new Random();

     int randomCards = (this.size*this.size) - ( ((this.size+1)/2) * ((this.size+1)/2) ) + 1; //Pocet karet, ktere se maji rozmistit nahodne
    
     bagOfCards = new MazeCard[randomCards]; // Init

     int cLeft = ((this.size*this.size)/3) - 4; // Pocet zbylych karet typu C[L]
     int fLeft = ((this.size*this.size)/3) - ( ((this.size/2)+1)*((this.size/2)+1) ) + 4; // Pocet zbylych karet typu F[T]
     int lLeft = ((this.size*this.size)/3); // Pocet zbylych karet typu L[I]
     int bagPos = 0;

     // Doplneni pomeru, zhruba 1:1:1
     if (((this.size*this.size)+1)%3 == 1) {
       lLeft++;
     }else if (((this.size*this.size)+1)%3 == 2) {
       lLeft++;
       cLeft++;
     }
  
     // Naplneni balicku zbyvajicimi kartami
     for ( ; 0 < cLeft ; cLeft--) { 
       card = MazeCard.create("C");
       card.luckyTurn();
       bagOfCards[bagPos] = card;
       bagPos++;
     }
      for ( ; 0 < fLeft ; fLeft--) { 
       card = MazeCard.create("F");
       card.luckyTurn();
       bagOfCards[bagPos] = card;
       bagPos++;
     }
      for ( ; 0 <  lLeft ; lLeft--) { 
       card = MazeCard.create("L");
       card.luckyTurn();
       bagOfCards[bagPos] = card;
       bagPos++;
     }

     // Zamichat balicek
     for(int i = 0; i < randomCards; i++){
        r = magic.nextInt(randomCards-1);
        if (i != r) {
          shuffleCard = bagOfCards[i];
          bagOfCards[i] = bagOfCards[r];
          bagOfCards[r] = shuffleCard;
        }
     }

     bagPos = 0;
     // Naplneni hraci plochy
     for(int i = 0; i < size; i++)
     {
        for(int j = 0; j < size; j++)
        {

          // Rohy hry
          if ( (i == 0 && j == 0) || (i == this.size-1 && j == 0) || (i == 0 && j == this.size-1) || (i == this.size-1 && j == this.size-1) ) {
            card = MazeCard.create("C");
            card.pointToGame(i+1, j+1, this.size);
            this.boardSpace[i][j].putCard(card);
          //Liche sloupce i rady  
          }else if ( i%2 == 0 && j%2 == 0 ) {// Place T, tak aby smerovali do hry           
            card = MazeCard.create("F");
            card.pointToGame(i+1, j+1, this.size);
            this.boardSpace[i][j].putCard(card);
          }else{
            // Vse ostatni, umistit zbyvajici L T I  
            this.boardSpace[i][j].putCard(bagOfCards[bagPos]);
            bagPos++;
          }

        }
     }
    this.freeCard = bagOfCards[bagPos];
    
    this.putTreasures();
    
  }
  
  /**
   * Funkce rozmisti po hraci plose (a volne karte) poklady.
   * Poklady jsou rozmisteny zcela nahodne a zadny kamen nemuze mit dva poklady.
   */
  public void putTreasures(){
        ArrayList<Integer> list = new ArrayList<>();
        int totalSize = (this.size*this.size) + 1;
        int row, col = 0;
        
        for (int i=0; i<totalSize; i++) {
            list.add(i);
        }
        Collections.shuffle(list);
        for (int i=0; i<this.treasureNumber; i++) {
            row = list.get(i)/this.size;
            col = list.get(i)%this.size;
            if (list.get(i) == totalSize -1 ){
                this.freeCard.setTreasure( Treasure.getTreasure(i));
            }else{
                this.boardSpace[row][col].getCard().setTreasure( Treasure.getTreasure(i));
            }
            
        }
  }
  
  /**
   * Fuknce vraci MazeField z pozice [r][c].
   * @param r - radek zadaneho pole.
   * @param c - sloupec zadaneho pole-
   * @return pozadovany MazeField
   */
  public MazeField get(int r, int c){
    if (r < 1 || r > this.size || c < 1 || c > this.size) {
      return null;
    }
    return this.boardSpace[r-1][c-1];
  }
  
  /**
   * Funkce vrati volnou kartu.
   * @return volna karta MazeCard
   */
  public MazeCard getFreeCard(){
      return this.freeCard;
  }
  
  /**
   * Zajistuje rotaci radku vkladane karty.
   * Nejprve se zkontroluje, zda je mozne rotaci provest a pokud je tomu tak,
   * provede se. V opacnem pripade se nedeje nic.
   * pro spusteni rotace je nutne vlozit kamen na sudy okraj hraci plochy.
   * Nejspis bude treba osetrit, ABY SE NEROTOVALY ROHY.
   * @param mf shiftovane pole
   */
  public void shift(MazeField mf){

    MazeCard temp;
    MazeField tempField;
    
    if(mf.col() > 0 && mf.col() <= this.size && mf.row() > 0 && mf.row() <= this.size)
        Game.setShifted(mf);

    if(mf.row() == 1 && (mf.col()%2)==0){
        for(int i = 1 ; i <= size ; i++){
           tempField = this.get(i,mf.col());  
           temp = tempField.getCard();
           tempField.putCard(freeCard);
           this.freeCard = temp;
        }
        for(int i = 0; i < Game.getNumberOfPlayers();i++) {
            Player p = Game.getPlayer(i);
            if(p.getLocation().col() == mf.col()) {
                p.setLocation(get(p.getLocation().row()%this.size+1, mf.col()));
            }
        }
     
    }

    if(mf.row() == size && (mf.col()%2)==0){
        for(int i = size ; i >= 1 ; i--){
            temp = this.get(i,mf.col()).getCard();
            this.get(i,mf.col()).putCard(freeCard);
            this.freeCard = temp;
        }
        for(int i = 0; i < Game.getNumberOfPlayers();i++) {
            Player p = Game.getPlayer(i);
            if(p.getLocation().col() == mf.col()) {
                p.setLocation(get((p.getLocation().row()+this.size-2)%this.size+1, mf.col()));
            }
        }
    }

    if(mf.col() == 1 && (mf.row()%2)==0){
        for(int i = 1 ; i <= size ; i++){
            temp = this.get(mf.row(),i).getCard();
            this.get(mf.row(),i).putCard(freeCard);
            this.freeCard = temp;
        }
        for(int i = 0; i < Game.getNumberOfPlayers();i++) {
            Player p = Game.getPlayer(i);
            if(p.getLocation().row() == mf.row()) {
                p.setLocation(get(mf.row(), p.getLocation().col()%this.size+1));
            }
        }
    }

    if(mf.col() == size && (mf.row()%2)==0){
        for(int i = size ; i >= 1 ; i--){
            temp = this.get(mf.row(),i).getCard();
            this.get(mf.row(),i).putCard(freeCard);
            this.freeCard = temp;
        }
        for(int i = 0; i < Game.getNumberOfPlayers();i++) {
            Player p = Game.getPlayer(i);
            if(p.getLocation().row() == mf.row()) {
                p.setLocation(get(mf.row(), (p.getLocation().col()+this.size-2)%this.size+1));
            }
        }
    }
  }

  /**
   * Funkce vytiskne hraci plochu.
   * Byla implementovana pro text ui, uvidime co se s tim bude dit.
   */
  public void print()
  {
    // Vytiskni freeCard nazacatek, UX biatch!
    if (null != this.freeCard) {
      this.getFreeCard().print();
    }
    System.out.println();

    // Vytiskne hraci plochu
    for (int i=1 ; i <= this.size  ; ++i ) {
      for (int j=1 ; j <= this.size ; ++j ) {
        this.get(i, j).print();
      }
      System.out.println();     
    }    
    
    for(int i = 0; i <this.size; i++){
        for(int j = 0; j < this.size; j++){
            this.printTest(this.canGo[i][j]);
        }
        System.out.println();
    }
  }
  
  public void printTest(boolean response){
    if (response){
       System.out.print("*");
    }else{
       System.out.print("x");
    } 
  }
  
  /**
   * Podle zadanych souradnit zaznaci do canGo[][], kam muze aktualni hrac jit.
   * Nejprve je volana resetRoutes(), ktera vyresetuje pole canGo[][] na false.
   * Pote se zavola rekurzivni funkce markRoutes(strRow, strCol)
   * @param strRow - pocatecni radek, od ktereho chceme zjistit cestu
   * @param strCol - pocatecni sloupec, od ktereho chceme zjistit cestu.
   */
  public void showRoutes(int strRow, int strCol){
    this.resetRoutes();
    this.markRoutes(strRow, strCol);
  }
  
  /**
   * Zkontroluje mozne cesty a pokud se hrac muze pohnout na zadane pole vraci
   * true, jinak false.
   * @param destRow - radek, kam se chce hrac dostat
   * @param destCol - sloupec, kam se chce hrac dostat
   * @return true pokud je to mozne, jinak false
   */
  public boolean canGo(int destRow, int destCol){
    return this.canGo[destRow][destCol];
  }
  
  /**
   * Rekurzivni funkce, ktera kontroluje kam je mozne se ze zadaneho policka
   * pohnout. Pokud je mozne se pohnout a stejne tak je moznost z finalniho se 
   * dostat zpet, je funkce volana nad finalnim. 
   * Castecna obdoba seminkoveho vyplnovani, pro velikosti hry dostacujici.
   * @param strRow - pocatecni rada
   * @param strCol - pocatecni sloupec
   */
  public void markRoutes(int strRow, int strCol){
    
    if( strRow >= this.size || strRow < 0 || strCol >= this.size || strCol < 0)
        return;
      
    this.canGo[strRow][strCol] = true;
    
    if ( strRow > 0 && this.canGo[strRow-1][strCol] == false && this.boardSpace[strRow][strCol].getCard().canGo(MazeCard.CANGO.UP) && this.boardSpace[strRow-1][strCol].getCard().canGo(MazeCard.CANGO.DOWN)){
        this.markRoutes(strRow-1, strCol);
    }    
    if ( strRow < this.size-1 && this.canGo[strRow+1][strCol] == false && this.boardSpace[strRow][strCol].getCard().canGo(MazeCard.CANGO.DOWN) && this.boardSpace[strRow+1][strCol].getCard().canGo(MazeCard.CANGO.UP)){
        this.markRoutes(strRow+1, strCol);
    } 
    if ( strCol > 0 && this.canGo[strRow][strCol-1] == false && this.boardSpace[strRow][strCol].getCard().canGo(MazeCard.CANGO.LEFT) && this.boardSpace[strRow][strCol-1].getCard().canGo(MazeCard.CANGO.RIGHT)){
        this.markRoutes(strRow, strCol-1);
    }    
    if ( strCol < this.size-1 && this.canGo[strRow][strCol+1] == false && this.boardSpace[strRow][strCol].getCard().canGo(MazeCard.CANGO.RIGHT) && this.boardSpace[strRow][strCol+1].getCard().canGo(MazeCard.CANGO.LEFT)){
        this.markRoutes(strRow, strCol+1);
    }   
  }
  
  /**
   * Nastavi cele pole canGo[][] na false.
   * Je volana funkci showRoutes(strRow, strCol)
   */
  public void resetRoutes(){
      for(int i = 0; i< this.size; i++)
        for(int j = 0; j< this.size; j++ )
            this.canGo[i][j] = false;
  }
}
