package projekt.board;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.swing.ImageIcon;
import projekt.gui.ImgLoader;

import projekt.treasure.Treasure;

/**
 * Trida popisuje hraci kamen.
 * Obsahuje smery, kterymi je mozne jit,
 * symbol, vyjadrujici textove znazorneni dane karty,
 * typ karty [LIT],
 * otoceni karty (jeden turn = 90)°
 * a informaci, zda karta obsahuje poklad.
 * 
 * @author xstude21, xnosek10
 */
public class MazeCard{

    private List<CANGO> dir;
    private char symbol;
    private char type;
    private int turn;
    protected Treasure treasure; // Nektere karty obsahuji poklad

    // Smery, kam se muze vydat. Override zajistuje, ze se meni cyklicky
    public static enum CANGO {

        LEFT, UP, RIGHT, DOWN{
            @Override
            public CANGO next(){
                return LEFT;
            };
        };
        
        public CANGO next(){
            return values()[ordinal()+1];
        }
    }
    
    public Image getImg(){
        BufferedImage img;
        img = (BufferedImage) ImgLoader.getMazeCardImg(this.type);
        
        AffineTransform tx = AffineTransform.getQuadrantRotateInstance(this.turn, img.getWidth(null) / 2.0, img.getHeight(null) / 2.0);
        AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_BILINEAR);
        return op.filter(img, null);
    }
    
    public ImageIcon getIcon(){
        return new ImageIcon(this.getImg());
    }
    
    public void paint(Graphics2D g, int x, int y){
        Image img = this.getImg();
        AffineTransform tx = AffineTransform.getTranslateInstance(x,y);
        
        g.drawImage(this.getImg(), tx, null);
        
        // Jeste treba dokreslit poklady
        
    }
    
    public char getSymbol(){
        return this.symbol;
    }
    
    public char getType(){
        return this.type;
    }
    
    /**
     * Vytvori hraci karty daneho typu.
     * Karta bude vytvorena bez pokladu a jeji otoceni (turn) bude 0.
     * @param type - typ karty [C,L,F]
     * @return Hraci kamen.
     */
    public static MazeCard create(String type){
        
        MazeCard card = new MazeCard();        
        card.dir = new ArrayList<>();
        card.treasure = null;
        card.turn = 0;
        
        switch(type){
        case "C":
            card.dir.add(CANGO.LEFT);
            card.dir.add(CANGO.UP); 
            card.symbol = '╝';
            card.type = 'c';
            break;

        case "L":
            card.dir.add(CANGO.LEFT);
            card.dir.add(CANGO.RIGHT);
            card.symbol = '═';
            card.type = 'l';
            break;

        case "F":
            card.dir.add(CANGO.LEFT);
            card.dir.add(CANGO.RIGHT);
            card.dir.add(CANGO.UP);
            card.symbol = '╩';
            card.type = 'f';
            break;
            
        default:
            // Vyhod chybu, spatny typ
            throw new IllegalArgumentException("Incorrect type of mazecard.");
                    
        }

        return card;
    }

    /**
     * Zjistuje, jestli je mozne jit danym smerem.
     * @param dir - smer, ktery se zjistuje.
     * @return True, pokud tim smerem muze jit.
     */
    public boolean canGo(MazeCard.CANGO dir){
        return this.dir.contains(dir);
    }

    /**
     * Otoceni karty o 90°doprava.
     * Pro kazdy smer, ulozeny v dir a provede posunuti na dalsi, cyklicky
     * se tak toci.
     */
    public void turnRight(){
        for(int i = 0; i < this.dir.size(); ++i){
            this.dir.set(i, this.dir.get(i).next());          
        }
        this.turn = (this.turn+1)%4;
        // Much case, such switch
        switch(this.symbol){
            case '╝' :
                this.symbol = '╚';
                break;
            case '╚' :
                this.symbol = '╔';
                break;
            case '╔' :
                this.symbol = '╗';
                break;
            case '╗' :
                this.symbol = '╝';
                break;
            case '═' :
                this.symbol = '║';
                break;
            case '║' :
                this.symbol = '═';
                break;
            case '╩' :
                this.symbol = '╠';
                break;
            case '╠' :
                this.symbol = '╦';
                break;
            case '╦' :
                this.symbol = '╣';
                break;
            case '╣' :
                this.symbol = '╩';
                break;           
        }

    }

    /**
     * Zajisti, aby vsechny cesty kamenu mirily do hry.
     * 
     * @param row - radek kamene
     * @param col - sloupec kamene
     * @param size - velikost pole
     */
    public void pointToGame(int row, int col, int size){
        // Rohove karty
        while( (this.canGo(CANGO.LEFT) && col == 1)
            || (this.canGo(CANGO.RIGHT) && col == size)
            || (this.canGo(CANGO.UP) && row == 1)
            || (this.canGo(CANGO.DOWN) && row == size) )
        {
            this.turnRight();
        }
    }
    
    /**
     * Nahodne pootoceni karty.
     */
    public void luckyTurn(){
        int r;
        Random magic = new Random();
        r = magic.nextInt(3);  
        for(; r > 0; r--){
            this.turnRight();
        }       
    }

    /**
     * Vytisknuti hraci plochy do textoveho rozhrani.
     */
    public void print(){
        System.out.print(this.symbol);
        if ( this.treasure != null)
            System.out.print(this.treasure.toString());
        else{
            System.out.print("*");
        }
    }

    /**
     * Prida kameni poklad.
     * @param tr vkladany poklad.
     */
    public void setTreasure(Treasure tr){
        this.treasure = tr;
    }
    
    /**
     * Vrati poklad kamene.
     * @return poklad kamene.
     */
    public Treasure getTreasure(){
        return this.treasure;
    }

}
