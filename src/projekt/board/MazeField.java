package projekt.board;

import java.util.HashSet;
import java.util.Set;
import projekt.player.Player;

/**
 *  Trida popisuje jedno policko na hraci plose.
 * Policko obsahuje svoji pozici (sloupec, radek), kartu a pripadne hrace.
 * @author xstude21
 */
public class MazeField{
    
    final private int row;
    final private int col;
    private MazeCard card;
    private Set<Player> players;
    private boolean canGo;


    /**
     * Creator - inicializace MazeField.
     * Nastavi radek a sloupec, kamen zatim zustava prazdy.
     * @param row radek pole
     * @param col sloupec pole
     */
    public MazeField(int row, int col){
        this.row = row;
        this.col = col;  
        this.card = null;
        players = new HashSet<>();
    }
    
    /**
     * Vrati radek pole.
     * @return radu.
     */
    public int row(){
        return this.row;
    }
    
    /**
     * Vrati sloupec pole.
     * @return sloupec
     */
    public int col(){
        return this.col;
    }
    
    /**
     * Vrati kamen, ktery se na poli nachazi.
     * @return kartu, na tomto poli.
     */
    public MazeCard getCard(){
        return this.card;
    }

    /**
     * Vlozi na pole kamen.
     * @param c - vkladany kamen
     */
    public void putCard(MazeCard c){
        this.card = c;
    }

    public boolean hasPlayers(){
        return !players.isEmpty();
    }
    
    public boolean hasPlayer(Player player){
        return players.contains(player);
    }
    
    public void removePlayer(Player player){
        players.remove(player);
    }
    
    public void addPlayer(Player player){
        players.add(player);
    }
    
    public boolean canGo(){
        return this.canGo;
    }
    
    public void setCanGo(){
        this.canGo = true;
    }
    
    public void unsetCanGo(){
        this.canGo = false;
    }
    
    /**
     * Vytiskne symbol karty nebo mezeru.
     */
    public void print()
    {
        if ( null != this.getCard() ) {
           this.getCard().print(); 
        }else{
            System.out.print(" ");
        }
    }
    
}
