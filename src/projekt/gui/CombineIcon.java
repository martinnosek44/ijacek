package projekt.gui;

import java.awt.Component;
import java.awt.Graphics;
import javax.swing.Icon;

/**
 * Trida pro kombinovani dvou ikon do jedne.
 * Vyuzivane predevsim pri vykreslovani pole, pro kombinaci hracich kamenu a
 * pokladu.
 * @author xstude21
 */
public class CombineIcon implements Icon{
    private Icon top;
    private Icon bottom;

    public CombineIcon(Icon top, Icon bottom) {
        this.top = top;
        this.bottom = bottom;
    }

    @Override
    public int getIconHeight() {
        return Math.max(top.getIconHeight(), bottom.getIconHeight());
    }

    @Override
    public int getIconWidth() {
        return Math.max(top.getIconWidth(), bottom.getIconWidth());
    }

    @Override
    public void paintIcon(Component c, Graphics g, int x, int y) {
        bottom.paintIcon(c, g, x, y);
        top.paintIcon(c, g, x + ((bottom.getIconWidth()-top.getIconWidth())/2), y + ((bottom.getIconHeight()-top.getIconHeight())/2));
    }

}