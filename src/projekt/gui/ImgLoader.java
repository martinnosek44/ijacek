package projekt.gui;


import java.awt.Color;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 * Trida zajistuje nahravani obrazku potrebnych pro hru.
 * Obsahuje funkce pro ziskani obrazku hernich komponent a uchovava je.
 * mazeCard[] - uchovava obrazky hracich kamenu
 * treasure[] - uchovava obrazky pokladu
 * arrow - uchovava sipku
 * imgPath - cesta do slozky s obrazky
 *
 * @author xstude21
 */
public class ImgLoader{
    
    private static final String imgPath = "lib/img/";
    private static Image[] mazeCard = null;
    private static Image[] treasure = null;
    private static Image arrow = null;
    private static Image turn = null;
    private static Image treasureFrame = null;
    
    private static final Color playerColor[] = {
        new Color(211, 84, 0),
        new Color(38, 194, 129),
        new Color(244, 179, 80),
        new Color(27, 163, 156)
    };
    private static final String playerName[] = { "BURNT ORANGE", "JUNGLE GREEN", "CASABLANCA", "LIGHT SEA GREEN" };
    
    /**
     * Vrati obrazek hraciho kamene.
     * Pokud jeste nejsou nahrane, nahraje obrazky MazeCard a pote vraci
     * pozadovy typ.
     * @param type typ MazeCard
     * @return Obrazek MazeCard
     */
    public static Image getMazeCardImg(char type){
        if(mazeCard == null){
            mazeCard = new Image[3];
            try{
                mazeCard[0] = ImageIO.read(new File(imgPath + "card_c_type.png")); 
                mazeCard[1] = ImageIO.read(new File(imgPath + "card_l_type.png")); 
                mazeCard[2] = ImageIO.read(new File(imgPath + "card_f_type.png")); 
            } catch (IOException e){
                mazeCard = null;
                System.err.println("MazeCard img not found.");
            }
        }
        switch(type){
            case 'c':
                return mazeCard[0];
            case 'l':
                return mazeCard[1];
            case 'f':
                return mazeCard[2];
            default:
                System.err.println("Vlozen spatny typ mazeCard.");
                return mazeCard[0];
        }
        
    }
 
    /**
     * Nacte obrazky pokladu do treasure[].
     * Obrazky jsou ulozene v imgPath.
     * @param index Kod pokladu.
     * @return obrazek pokladu s indexem index.
     */
    public static Image getTreasureImg(int index){
        if( treasure == null){
            treasure = new Image[24];
            for( int i = 0; i < treasure.length; i++){
                try {
                    treasure[i] = ImageIO.read(new File(imgPath + "t" + i + ".png"));
                } catch (IOException e) {
                    System.err.println("Failed loading Treasure's Images");
                }
            }
        }
        return treasure[index];
    }    
    
    /**
     * Ziska obrazek sipky.
     * Nacte obrazek ze slozky imgPath.
     * @return Obrazek sipky
     */
    public static Image getArrow(){
        
        if(arrow == null){
            try{
                arrow = ImageIO.read(new File(imgPath + "arrow.png"));
            } catch (IOException e){
                System.err.println("Loading arrow.png failed.");
            }
        }
        return arrow;
    }
    
    public static Image getTurn(){
        if(turn == null){
            try{
                turn = ImageIO.read(new File(imgPath + "turnRight.png"));
            } catch (IOException e){
                System.err.println("Loading turnRight.png failed.");
            }
        }
        return turn;      
    }
    
    public static Image getTreasureFrame(){
        if(treasureFrame == null){
            try{
                treasureFrame = ImageIO.read(new File(imgPath + "treasureFrame.png"));
            } catch (IOException e){
                System.err.println("Loading treasureFrame.png failed.");
            }
        }
        return treasureFrame;      
    }
    
    public static Color getPlayerColor(int index){
        return playerColor[index];
    }
    
    public static String getPlayerName(int index){
        return playerName[index];
    }
    
    /**
     * Vrati sirku MazeCard.
     * @return Sirka MazeCard
     */
    public static int getCardWidth(){
        return getMazeCardImg('c').getWidth(null);
    }
    /**
     * Vrati vysku MazeCard.
     * @return Vyska MazeCard
     */
    public static int getCardHeight(){
        return getMazeCardImg('c').getHeight(null);
    }
    
}
