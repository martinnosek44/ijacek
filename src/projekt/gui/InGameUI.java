package projekt.gui;

import projekt.Game;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.PopupMenu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import projekt.History;

import projekt.board.MazeBoard;
import projekt.board.MazeCard;
import projekt.board.MazeField;
import projekt.player.Player;

/**
 *  Trida predstavuje graficke rozhrani ve hre, tedy pote, co se nastavi pocet 
 *  hracu, velikost pole, pokladu a hra se spusti.
 * Hraci stul a vkladaci plochy jsou implementovany polem buttonu. UI je pote 
 * pridano kombinaci JLabelu, ty jsou seskupeny ve funkci createMenuPanel().
 * Pro nacitani obrazku je vyuzito tridy ImgLoader.
 * @author xstude21
 */
public class InGameUI implements ActionListener {
    private static MazeBoard board;
    private static JFrame frame;
    private static JButton button[][];
    
    private MazeField field;
    private static JLabel cardLabel;
    private static JLabel treasureLabel;
    private static JLabel playerLabel;
    private static JLabel scoreLabel[];
    private static JButton save;
    
    /**
     * Creator, nastavi hraci pole.
     */ 
    public InGameUI(){
        field = null;
    }
    public InGameUI(MazeField field){
        this.field = field;   
    }
   
    
    /**
     * Nastaveni herniho boardu.
     * @param board board pro tuto hru
     */
    public static void setBoard(MazeBoard board){
       InGameUI.board = board;
    }
    
    /**
     * Vykresleni herniho rozhrani.
     * Je volan novy JFrame a zaroven je inicializovan.
     * Pote mu jsou pridany dalsi JPanely, ktere dohromady skladaji herni GUI.
     */
    public static void runInGameUI(){
        frame = new JFrame("ArciLabyrint");
        frame.setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.LINE_AXIS));
        
        frame.add(createBoard());
        frame.add(createMenuPanel());
        
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);   // Zavirani 
        frame.setLocation(0, 0); 
        frame.pack();   // Prizpusobi velikost okna - tedy, pokud nemate retinu.
        frame.setVisible(true);
        
    }
    
    /**
     * Vytvoreni herniho boardu a take sipek na vkladani kolem nej.
     * Na novy JPanel je nastaven GridLayout. 
     * Samotna hraci plocha je pote 2D pole buttonu, kolem ktere jsou na sudych
     * radsich vkladaci plochy pro shiftovani.
     * Buttony jsou inicializovany a pripraveny k vykresleni.
     * Vkladacim plocham je zaroven nastaven hoverovy stav mysi, kde se vykresli volna
     * karta.
     * @return JPanel inicializovaneho boardu se sipkama pro shiftovani.
     */
    public static JPanel createBoard(){
        JPanel panel = new JPanel();
        //System.out.println("Card width & height "+ImgLoader.getCardWidth() +" "+ ImgLoader.getCardHeight());
        panel.setLayout(new GridLayout(board.getSize()+2, board.getSize()+2));
        
        button = new JButton[board.getSize()][board.getSize()];
        
        for(int i = 0; i < board.getSize() + 2; i++)
        {
            for(int j = 0; j < board.getSize() + 2; j++)
            {
                if(i>0 && j>0 && i<=board.getSize() && j<=board.getSize())
                {   // Zobrazeni hraciho boardu
                    button[i-1][j-1] = new JCheckedButton(board.get(i, j));
                    flatButton(button[i-1][j-1]);
                    setCard(button[i-1][j-1], board.get(i, j).getCard());
                    button[i-1][j-1].setSize(ImgLoader.getCardWidth(), ImgLoader.getCardHeight());                 
                    button[i-1][j-1].addActionListener(new InGameUI(board.get(i, j)));
                    button[i-1][j-1].setActionCommand("MOVE");    
                    panel.add(button[i-1][j-1]);
                }else if ( i%2 == 0 && j%2 == 0 && ( i%(board.getSize()+1) != 0 || j%(board.getSize()+1) != 0))
                {   // Vkladaci plochy
                    JButton arrowButton = new JButton(); // I'm no Oliver Queen!
                    flatButton(arrowButton);
                    arrowButton.setIcon(new ImageIcon(ImgLoader.getArrow())); //Detective Lance, let it be!
                    arrowButton.setSize(ImgLoader.getCardWidth(), ImgLoader.getCardHeight());
                    if (i == 0)
                        arrowButton.addActionListener(new InGameUI(board.get(i+1,j)));
                    if (i == board.getSize() + 1)
                        arrowButton.addActionListener(new InGameUI(board.get(i-1, j)));
                    if (j == 0)
                        arrowButton.addActionListener(new InGameUI(board.get(i, j+1)));
                    if (j == board.getSize() + 1)
                        arrowButton.addActionListener(new InGameUI(board.get(i, j-1)));
                    
                    
                    arrowButton.addMouseListener(new java.awt.event.MouseAdapter()
                    {
                        @Override
                        public void mouseEntered(java.awt.event.MouseEvent evt){
                            if(!Game.playerShiffted)
                                setCard(arrowButton, board.getFreeCard());
                        }
                        
                        @Override
                        public void mouseExited(java.awt.event.MouseEvent event){
                            arrowButton.setIcon(new ImageIcon( ImgLoader.getArrow() ));
                        }
                        
                        @Override
                        public void mouseClicked(java.awt.event.MouseEvent evt){
                            setCard(arrowButton, board.getFreeCard());
                        }
                    });
                    arrowButton.setActionCommand("SHIFT");
                    panel.add(arrowButton);                   
                }else{
                    panel.add(new JLabel());
                }
            }
        }
        updateBoard();
        
        return panel;
    }

    /**
     * Prekresli potrebne hraci kameny boardu.
     * Prekresli barvy a obrazky hracich kamenu.
     */
    public static void updateBoard(){      
        for(int i = 0; i < board.getSize(); ++i){
            for(int j = 0; j < board.getSize(); ++j){
                setCard(button[i][j], board.get(i+1, j+1).getCard());
            }
        }          
    }
    
   /**
    * Nastaveni pozice textu JLabelu.
    * @param label - JLabel, ktery ma ybt nastaven.
    */   
    private static void labelInit(JLabel label){
        label.setHorizontalTextPosition(JButton.CENTER);
        label.setVerticalTextPosition(JButton.CENTER);
    }
    
    /**
     * Nastavi ikonu JButtonu nebo JLabelu.
     * Pokud je treba, skombinuje dve ikony do jedne volanim tridy CombineIcon
     * (napriklad pro poklad a kamen).
     * @param button JButton, nebo Jlabel, ktery ma byt nastaven.
     * @param card Kamen, podle ktereho se vybiraji ikony.
     */
    public static void setCard(JButton button, MazeCard card){
        if(card.getTreasure() != null)
            button.setIcon( new CombineIcon( new ImageIcon(ImgLoader.getTreasureImg(card.getTreasure().code)) , card.getIcon() ));
        else
            button.setIcon( card.getIcon());   
    }
    
    public static void setCard(JLabel label, MazeCard card){
        if(card.getTreasure() != null)
            label.setIcon( new CombineIcon( new ImageIcon(ImgLoader.getTreasureImg(card.getTreasure().code)) , card.getIcon() ));
        else
            label.setIcon( card.getIcon());   
    }
    
    private static void flatButton(JButton button){
        button.setBorder(null);
        button.setBorderPainted(false);
        button.setOpaque(false);
        button.setContentAreaFilled(false);
    }
    
    /**
     * Vytvoří JPanel volného hracího kamene.
     * Panel obsahuje obrazek tohoto kamene a take tlacitko na jeho otoceni.
     * @return JPanel s volnym hracim kamenem a otacenim.
     */
    public static JPanel createFreeCard(){
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.LINE_AXIS));
        
        cardLabel = new JLabel();
        labelInit(cardLabel);
        setCard(cardLabel, board.getFreeCard());
        cardLabel.setBorder(null);
        
        JButton turnButton = new JButton();
        turnButton.setIcon(new ImageIcon(ImgLoader.getTurn()));
        turnButton.addActionListener(new InGameUI());
        turnButton.setActionCommand("TURN");
        flatButton(turnButton);
        
        panel.add(turnButton);
        panel.add(Box.createRigidArea(new Dimension(18,0)));
        panel.add(cardLabel);
        
        return panel;   
    }
    
    /**
     * Vytvoří JPanel aktuálního pokladu, také vedle něj vykreslí aktuálmního
     * hráče.
     * @return JPanel s aktualnim pokladem a hracem.
     */
    public static JPanel createTreasure(){
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.LINE_AXIS));
        
        treasureLabel = new JLabel();
        updateTreasure();
        labelInit(treasureLabel);
        
        playerLabel = new JLabel();
        labelInit(playerLabel);
        playerLabel.setBackground(ImgLoader.getPlayerColor(Game.getActPlayer().getPosition()));
        playerLabel.setText("POKLÁDÁ");
        playerLabel.setBorder(new EmptyBorder(10,18,10,18));
        playerLabel.setForeground(Color.WHITE);
        playerLabel.setOpaque(true);
        
        panel.add(treasureLabel);
        panel.add(Box.createRigidArea(new Dimension(28,0)));
        panel.add(playerLabel);
        return panel;
    }
    
    /**
     * Vytvori vypis hracu a jejich skore (pocet ziskanych pokladu).
     * @return JPanel s hracema vypsanyma podsebou.
     */
    public static JPanel createScoreBoard(){
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(0,2));
        scoreLabel = new JLabel[Game.getNumberOfPlayers()];
        int points;
        
        for(int i = 0; i < Game.getNumberOfPlayers(); ++i){
            JLabel playerLabel = new JLabel(ImgLoader.getPlayerName(i)+ "  ");
            points = Game.getPlayer(i).getPoints();
            if (points < 0)
                points = 0;
            scoreLabel[i] = new JLabel(Integer.toString(points));
            panel.add(playerLabel);
            panel.add(scoreLabel[i]);
        }
        
        return panel;
    }
    
    /**
     * Vytvori JPanel s tlacitkem pro ulozeni hry.
     * @return Panel na ukladani.
     */
    public static JPanel createSave(){
        JPanel panel = new JPanel();
        save = new JButton("SAVE GAME");
        save.addActionListener(new InGameUI());
        save.setActionCommand("SAVE GAME");
        save.setBorder(new EmptyBorder(16,36,16,36));
        save.setBorderPainted(false);
        save.setOpaque(true);
        save.setBackground(new Color(34, 49, 63));
        save.setForeground(Color.WHITE);
        
        panel.add(save);
        return panel;
    }
    
    public static JPanel createUndo() {
        JPanel panel = new JPanel();
        JButton undo = new JButton("UNDO");
        undo.addActionListener(new InGameUI());
        undo.setActionCommand("UNDO");
        undo.setBorder(new EmptyBorder(16,36,16,36));
        undo.setBorderPainted(false);
        undo.setOpaque(true);
        undo.setBackground(new Color(34, 49, 63));
        undo.setForeground(Color.WHITE);
        
        panel.add(undo);
        return panel;   
    }
    
    /**
     * Aktualizuje obrazek hledaneho pokladu, nastavi jej na hledany poklad
     * aktualniho hrace.
     */
    public static void updateTreasure(){
        treasureLabel.setIcon( new ImageIcon(ImgLoader.getTreasureImg(Game.getActPlayer().getActTreasure().code)) );
    }
    
    /**
     * Aktualizuje pocet pokladu ziskanych jednotlivymi hraci.
     */
    public static void updateScore(){
        for(int i = 0; i < scoreLabel.length; i++){
            int points = Game.getPlayer(i).getPoints();
            if (points < 0)
                points = 0;
            scoreLabel[i].setText(Integer.toString(points));
        }
    }
    
    /**
     * Aktualizuje barvu hrace na aktualni a prepise na aktualni stav.
     */
    public static void updatePlayer(){
        if (Game.playerShiffted){        
            playerLabel.setText("HRAJE");
            playerLabel.setBackground(ImgLoader.getPlayerColor(Game.getActPlayer().getPosition()));
        }else{
            playerLabel.setText("POKLÁDÁ");
            playerLabel.setBackground(ImgLoader.getPlayerColor(Game.getActPlayer().getPosition()));
        }
    }
    
    /**
     * Vytvori JPanel pro ovladani hry.
     * Podsebou zobrazi: Volny hraci kamen, poklad, skore a tlacitko pro ulozeni.
     * @return JPanel ingame menu.
     */
    public static JPanel createMenuPanel(){
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
        
        panel.add(Box.createRigidArea(new Dimension(0,80)));
        panel.add(createFreeCard());
        panel.add(Box.createRigidArea(new Dimension(0,20)));
        panel.add(createTreasure());
        panel.add(createScoreBoard());
        panel.add(createSave());
        panel.add(createUndo());
        
        return panel;
    }
    
    /**
     * Nastavi funkce k jednotlivym zaznamenanym eventum.
     * @param e provedeny event
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String action = e.getActionCommand();
        
        switch (action){
            case "MOVE":
                if(board.canGo(field.row()-1, field.col()-1) && Game.playerShiffted){
                   History.move(Game.getActPlayer(), Game.getBlocked(), field);
                   Game.getActPlayer().setLocation(field);
           
                   Game.playerNext();
                   
                   InGameUI.updateBoard();
                   InGameUI.updatePlayer();
                   InGameUI.updateScore();
                   InGameUI.updateTreasure();
                }
                InGameUI.updateBoard();
                break;
            case "SHIFT":
                if (Game.canShift(field)){
                    InGameUI.board.shift(field);
                    History.shift(field, board.getFreeCard().getSymbol());
                    board.showRoutes(Game.getActPlayer().getLocationRow()-1, Game.getActPlayer().getLocationCol()-1);
                    setCard(cardLabel, board.getFreeCard());
                    //updateArrow();
                    InGameUI.updateBoard();
           
                    InGameUI.updatePlayer();
                }
                break;
            case "TURN":
                InGameUI.board.getFreeCard().turnRight();
                setCard(cardLabel, board.getFreeCard());
                break;
                
            case "SAVE GAME":
                Game.saveGame();
                break;
            case "UNDO":
                History.undo();
                setCard(cardLabel, board.getFreeCard());
                board.showRoutes(Game.getActPlayer().getLocationRow()-1, Game.getActPlayer().getLocationCol()-1);
              
                InGameUI.updateBoard();
                InGameUI.updatePlayer();
                InGameUI.updateScore();
                InGameUI.updateTreasure();
                
                break;
        }
    }
    
    /**
     * Ukonceni hry a vypsani viteze.
     * Vytvori se JOptionPane, kde se vypise vitez, pote se hra ukonci.
     * @param player - vitezny hrac.
     */
    public static void playerWon(Player player) {
        System.out.println(ImgLoader.getPlayerName(player.getPosition())+" is WINNER!");
        Object[] options = {"End game & celebrate your glorious victory!"};
        String msg = ImgLoader.getPlayerName(player.getPosition())+ " is *WINNER*\n\n";
        // Vypsani bodu ostatnich hracu
        for (int i = 0; i < Game.getNumberOfPlayers(); ++i){
            if (i != player.getPosition())
                msg += ImgLoader.getPlayerName(i)+" got "+Game.getPlayer(i).getPoints()+" Treasures.\n";
        }
        int option;
        option = JOptionPane.showOptionDialog(frame,
                msg ,
                ImgLoader.getPlayerName(player.getPosition())+ " is 100% 1337!",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.INFORMATION_MESSAGE,
                null,
                options,
                options[0]);
        frame.setVisible(false);
        frame.dispose();
        //if(option == 0) {
        //    Settings.visible();
        //} else {
            frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
        //}
    }
    
    /**
     * Vykresleni GUI.
     */
    public static void run(){
        System.out.println(board.getSize());
        SwingUtilities.invokeLater(() -> {
            runInGameUI();
        });       
    }
       
    /**
     * Class pro vykreslování hráčů.
     */
    private static final class JCheckedButton extends JButton{
        private final MazeField field;
       
        private JCheckedButton(MazeField mf){
            super("");
            setContentAreaFilled(true);
            setFocusPainted(false); // used for demonstration
            field = mf;
        }
        
        @Override
        protected void paintComponent(Graphics g){

            if ( field.hasPlayers() ){
                Graphics2D g2 = (Graphics2D)g.create();
                int width = getWidth()/4;
                int height = getHeight()/4;
  
                if(field.hasPlayer(Game.getPlayer(0))){
                    g2.setColor( ImgLoader.getPlayerColor(0));
                    g2.fillRect(width, height, width, height);
                }
                if(field.hasPlayer(Game.getPlayer(1))){
                    g2.setColor( ImgLoader.getPlayerColor(1));
                    g2.fillRect(width*2, height, width, height);
                }
                if(field.hasPlayer(Game.getPlayer(2))){
                    g2.setColor( ImgLoader.getPlayerColor(2));
                    g2.fillRect(width*2, height*2, width, height);
                }
                if(field.hasPlayer(Game.getPlayer(3))){
                    g2.setColor( ImgLoader.getPlayerColor(3));
                    g2.fillRect(width, height*2, width, height);
                }
                
                g2.dispose();
            }
            
            super.paintComponent(g);

        }

    }
    
}
