/*
 */
package projekt.player;

import projekt.treasure.TreasureCard;
import projekt.board.MazeBoard;

import java.util.ArrayList;
import java.util.List;
import projekt.Game;
import projekt.board.MazeField;
import projekt.treasure.CardPack;
import projekt.treasure.Treasure;

/**
 * Trida Player popisujes hrace labirintu. Hrac vi, kde se nachazi na mape a take kolikaty
 * je v poradi. Take nese informace o tom, kolik ma bodu a jaky poklad prave hleda.
 *
 * @author xstude21
 */
public class Player{

  private MazeField location;
  private int position;
  private int points;
  public TreasureCard actTreasure;
  public List<TreasureCard> playedTreasures;
  
  public static int numberOfPlayers = 0;

  /**
   * Vytvoří nového hráče 
   * Přiřadí mu pozici
   * Hrac zacina s -1 bodem. S prvním líznutím karty se tato hodnota zmeni na 0.
   * @param field - policko, na kterem se hrac nachazi
   */
  public Player(MazeField field){
  	this.position = Player.numberOfPlayers++;
  	this.location = field;
  	this.points 	 = 0; 
        this.actTreasure = null;
        this.playedTreasures = new ArrayList<>();
        field.addPlayer(this);
  }

  /**
   * Nastavi lokaci hraci
   * @param field cilove pole
   */
  public void setLocation(MazeField field){
  	this.location.removePlayer(this);
        this.location = field;
        field.addPlayer(this);
  }
  
  public MazeField getLocation(){
      return this.location;
  }

  /**
   * Nastavi pozici hrace, musi byt v rozmezi 1-4, to jest pocet hracu pro tuto hru.
   * @param position - pozice
   */
  public void setPosition(int position){
  	if (position <=4 && position >= 1)
  		this.position = position;
  }

  /**
   * Vrati poradi hrace.
   * @return  Poradi hrace
   */
  public int getPosition(){
  	return this.position;
  }
  
  /**
   * Posune hrace na vybranou lokaci a nastavi mu nove souradnice.
   * @param board hraci plocha, po ktere se pohybuje
   * @param field cilove pole
   */
  public void goTo(MazeBoard board, MazeField field){
    this.location.removePlayer(this);
    this.location = field;
    field.addPlayer(this);

  }
  /**
   * Vrati cislo radku, na kterem se hrac nachazi
   * @return  Cislo radku
   */
  public int getLocationRow(){
      return this.location.row();
  }
  
  /**
   * Vrati cislo sloupce, na kterem se hrac nachazi
   * @return  Cislo sloupce
   */
  public int getLocationCol(){
      return this.location.col();
  }
  
  public Treasure getActTreasure(){
      if (this.actTreasure != null)
        return this.actTreasure.getTreasure();
      return null;
  }
  
  public int getPoints(){
      return points;
  }
  
  public void checkTreasure(){
      if (location.getCard().getTreasure() == actTreasure.getTreasure()){
          playedTreasures.add(actTreasure);
          this.points += 1;
          actTreasure = null;
      }
  }
  
  public void dropTreasure(){
      if(actTreasure != null)
          Game.pack.pushCard();
          
      setActTreasure(playedTreasures.get(playedTreasures.size()-1));
      playedTreasures.remove(playedTreasures.size()-1);
      points--;
  }
  
  
  public void setActTreasure(TreasureCard card){
      actTreasure = card;
  }
  
  /**
   * Lizne si kartu z balicku
   * pricte bod a ulozi zahranou kartu do playedTreasure
   * @param card - Nova karta pokladu z balicku
   */
  public void getCard(TreasureCard card){
  	if (this.actTreasure != null)
            this.playedTreasures.add(this.actTreasure);
  	//this.points += 1;
  	this.actTreasure = card;
  }
  
  public boolean won(){
      System.out.println("points " +this.points+ " Maxsize "+CardPack.maxSize+" players "+Game.player.size());
      return(this.points) == (Game.pack.maxSize/Game.player.size());
  }
  

}
