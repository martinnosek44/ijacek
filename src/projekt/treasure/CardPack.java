package projekt.treasure;
import java.util.Random;

/**
 * Balicek hracich karet s pokladama.
 * 
 * @author xstude21
 */
public class CardPack implements java.io.Serializable{
	public TreasureCard[] packofCards;
	protected int currentSize;
	static public int maxSize;

	public CardPack(int maxS, int initSize){
            if (initSize <= maxS) {
            this.packofCards = new TreasureCard[maxS];
			for (int i = initSize - 1; i >= 0 ; i-- ) {
				this.packofCards[i] = new TreasureCard(Treasure.getTreasure(initSize-i-1));
			}
			this.currentSize = initSize; 
		}
            maxSize = maxS;
	}
	public TreasureCard popCard(){
		this.currentSize--;
		return this.packofCards[this.currentSize];
	}
        public void pushCard(){
            this.currentSize++;
        } 
	public int size(){
		return this.currentSize;
	}
        
	public void shuffle(){	
		Random rand = new Random();
		int shuffle;
		TreasureCard pomCard;

		for (int i = 0; i < currentSize ; i++ ) {
			shuffle = rand.nextInt(currentSize);
			pomCard = this.packofCards[i];
			this.packofCards[i] = this.packofCards[shuffle];
			this.packofCards[shuffle] = pomCard;
		}
	}
}