package projekt.treasure;

/**
 * Poklady, jejich pole je aplikovane jako public static, muzeme se k nim tak
 * dostat pres tridu.
 * 
 * @author xstude21
 */
public class Treasure{

	public int code;
	static int objectNumber;
	public static Treasure[] treasures;

	private Treasure(int code){
		this.code = code;
	}
        
        public int getObjectNumber(){
            return Treasure.objectNumber;
        }

	public static void createSet(int size){
            Treasure.objectNumber = size;
            Treasure.treasures =  new Treasure[ objectNumber ];
		for (int i=0; i < objectNumber; i++ ) {
			Treasure.treasures[i] = new Treasure(i);
		}
	}

	public static Treasure getTreasure(int code){
            if (code < Treasure.objectNumber && code >= 0 ){
                return Treasure.treasures[code];                
            }else{
                return null;
            }
	}
        
        @Override
        public String toString(){
            return Integer.toString(code);
        }
}