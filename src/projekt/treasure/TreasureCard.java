package projekt.treasure;

import java.util.Objects;

/**
 * Karta pokladu, volana v CardPack, pro vytvoreni balicku.
 * 
 * @author xstude21
 */
public class TreasureCard {

    protected Treasure treasure;
	
    public TreasureCard(Treasure tr){
        this.treasure = tr;
    }
    
    public Treasure getTreasure(){
        return this.treasure;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 79 * hash + Objects.hashCode(this.treasure);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TreasureCard other = (TreasureCard) obj;
        if (!Objects.equals(this.treasure, other.treasure)) {
            return false;
        }
        return true;
    }

        

}

