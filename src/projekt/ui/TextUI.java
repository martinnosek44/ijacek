/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package projekt.ui;

import projekt.board.MazeBoard;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * User interface to the system.
 * @author xstude21, xnosek10
 * @version 1.0, 6 Feb 2014
 */
public class TextUI {
    
    private final MazeBoard board;

    /**
     * Constructor.
     * @param b board the UI communicates to.
     */
    public TextUI(MazeBoard b) {
        this.board = b;
    }
    
    /**
     * Starts a process of reading commands from standard input and their processing. 
     * It calls the method {@link #processString} to process and execute commands.
     */
    public void start() {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        boolean end = false;
        
        while (! end) {
            try {
                System.out.print("Prompt>");
                String str = in.readLine();
                end = processString(str);
            } catch (IOException ex) {
                Logger.getLogger(TextUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Analyses the input string and executes the command.
     * If the command does not exist or the string is invalid, the method does nothing.
     * @param str Input string
     * @return Returns true if the command end was typed; otherwrise it returns false.
     */
    protected boolean processString(String str) {
        boolean end = false;
        switch(str) {
            case "q" :
                end = true;
                break;
            case "p" :
                // Zobrazi hraci desku
                board.print();
                break;
            case "n" :
                board.newGame();
                // newGame
                break;
            default:

                // String to be scanned to find the pattern.
              String pattern = "[s]([0-9])([0-9])";

              // Create a Pattern object
              Pattern p = Pattern.compile(pattern);

              // Now create matcher object.
              Matcher m = p.matcher(str);

              if ( m.find() ) {
                int r = Integer.parseInt(m.group(1));
                int c = Integer.parseInt(m.group(2));

                if ( null == board.get(r, c) ) {
                    System.err.println("Oh boy, there is no such field ["+r+","+c+"].");
                }else{
                    board.shift( board.get(r, c) );
                }
              } else {
                 System.err.println("Invalid Argument");
              }

            break;
            
        }
        return end;
    }
}
